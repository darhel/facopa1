<?php
  return 
   [
     ["nom"=>"Creation","table"=>"Membres"],
     ["nom"=>"Ajout","table"=>"Membres"],
     ["nom"=>"Modification personnelle","table"=>"Membres"],
     ["nom"=>"Modification générale","table"=>"Membres"],
     ["nom"=>"Suppression personnelle","table"=>"Membres"],
     ["nom"=>"Suppression générale","table"=>"Membres"],
     ["nom"=>"Liste","table"=>"Membres"],
     ["nom"=>"voir","table"=>"Membres"],

     ["nom"=>"Ajout","table"=>"Entreprises"],
     ["nom"=>"Modification personnelle","table"=>"Entreprises"],
     ["nom"=>"Modification générale","table"=>"Entreprises"],
     ["nom"=>"Suppression personnelle","table"=>"Entreprises"],
     ["nom"=>"Suppression générale","table"=>"Entreprises"],
     ["nom"=>"Liste","table"=>"Entreprises"],
     ["nom"=>"voir","table"=>"Entreprises"],

     ["nom"=>"Ajout","table"=>"projets"],
     ["nom"=>"Modification personnelle","table"=>"projets"],
     ["nom"=>"Modification générale","table"=>"projets"],
     ["nom"=>"Suppression personnelle","table"=>"projets"],
     ["nom"=>"Suppression générale","table"=>"projets"],
     ["nom"=>"Voir","table"=>"projets"],
     ["nom"=>"Liste","table"=>"projets"],

     ["nom"=>"Ajout","table"=>"tasks"],
     ["nom"=>"Modification personnelle","table"=>"tasks"],
     ["nom"=>"Modification générale","table"=>"tasks"],
     ["nom"=>"Suppression personnelle","table"=>"tasks"],
     ["nom"=>"Suppression générale","table"=>"tasks"],
     ["nom"=>"Liste","table"=>"tasks"],

     ["nom"=>"Ajout","table"=>"comments"],
     ["nom"=>"Suppression personnelle","table"=>"comments"],
     ["nom"=>"Suppression générale","table"=>"comments"],
     ["nom"=>"Liste","table"=>"comments"],

     ["nom"=>"Ajout","table"=>"users"],
     ["nom"=>"Modification","table"=>"users"],
     ["nom"=>"Suppression","table"=>"users"],
     ["nom"=>"Liste","table"=>"users"],

     ["nom"=>"Ajout","table"=>"roles"],
     ["nom"=>"Modification","table"=>"roles"],
     ["nom"=>"Suppression","table"=>"roles"],
     ["nom"=>"Liste","table"=>"roles"],
     
     ["nom"=>"Ajout","table"=>"permissions"],
     ["nom"=>"Modification","table"=>"permissions"],
     ["nom"=>"Suppression","table"=>"permissions"],
   ] 
?>