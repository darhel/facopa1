@extends('layouts.app')

@section('content')
<login  inline-template v-cloak>
    <div class="login-box" >
      <div class="login-logo">
        <a href="javascript:void(0)">
          <img src="{{asset('img/logo_facop.png') }}" alt="" class="mb-2">
        </a>
      </div>
      <!-- /.login-logo -->
      <div class="card" style="border-radius: 30px;">
        <div class="card-body login-card-body">
          <p class="login-box-msg">Connectez-vous pour lancer une session</p>
          <form method="POST" action="/login" @submit.prevent="onSubmit()"
                               @reset.prevent="reset()"
                               @keydown="form.errors.clear($event.target.name)"
                               @change="form.errors.clear($event.target.name)">
            <div class="input-group mb-3">
                <input @input="updateError" placeholder="Entrez votre e-mail ou votre username" :class="['form-control form-control-sm', (this.form.errors.has('email') )? ' is-invalid' : '']" name="email" v-model="form.email" type="text" />
                <div class="input-group-append">
                            <div class="input-group-text">
                              <span class="fas fa-envelope"></span>
                            </div>
                 </div>
                  <span class="m-form__help m--font-danger invalid-feedback " v-if="form.errors.has('email') || form.errors.has('username') || this.form.errors.has('email')" v-text="form.errors.get('email') || form.errors.get('username') || this.form.errors.get('email')"></span>
            </div>
            <div class="input-group mb-3">
                    <input placeholder="Entrez votre mot de passe" :class="['form-control form-control-sm', form.errors.has('password')? ' is-invalid' : '']" name="password" v-model="form.password" :type="show? 'password' : 'text'" />
                    <div class="input-group-append">
                        <button class="input-group-text" type="button" @click="show">
                          <span class="fas fa-eye"></span>
                        </button>
                    </div>
                 <span class="m-form__help m--font-danger invalid-feedback " v-if="form.errors.has('password')" v-text="form.errors.get('password')"></span>
            </div>
            <div class="align-items-center d-flex justify-content justify-content-between">
              <div class="">
                <div class="icheck-primary">
                  <input type="checkbox" id="remember">
                  <label for="remember">
                    Se souvenir de moi
                  </label>
                </div>
              </div>
              <!-- /.col -->
              <div class="">
                <button style="min-height: 38px;min-width:74px" type="submit" :class="['btn btn-primary btn-block mr-2',isLoading ? 'm-loader m-loader--sm m-loader--light m-loader--right' :'']" :disabled="form.errors.any()">@{{ isLoading ? "Login" : 'Login' }}</button>
              </div>
              <!-- /.col -->
            </div>
          </form>
          <!-- /.social-auth-links -->
{{--
          <p class="mb-0 pb-0">
            <a href="{{ route('password.request') }}"><small>Mot de passe oublié ?</small></a>
          </p>  --}}
          <p class="mb-1">
            <a href="{{ route('register') }}"><small>Créer un nouveau compte</small></a>
          </p>
        </div>
        <!-- /.login-card-body -->
      </div>
    </div>
</login>

@endsection
