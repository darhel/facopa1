<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;

$factory->define(App\Pays::class, function (Faker $faker) {
    return [
        'libelle' => $faker->word,
        'code1' => $faker->countryCode(),
        'code2' => $faker->countryCode(),
    ];
});
