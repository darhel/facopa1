<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Config;


class FormationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $formations = Config::get('formations');

        foreach($formations as $formation)
        {
            $a = ["libelle"=>$formation['libelle']];
            factory('App\Formation')->create($a);
        }
    }
}
