<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Config;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = Config::get('permissions');

        foreach($permissions as $permission)
        {
            factory('App\Permission')->create(["nom"=>$permission['nom'],"table"=>$permission['table']]);
        }
    }
}
