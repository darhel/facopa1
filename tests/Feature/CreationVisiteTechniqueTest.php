<?php

namespace Tests\Feature;

use App\Option;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class CreationVisiteTechniqueTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @var \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model
     */
    private $vehicule;

    protected function setUp(): void
    {
        parent::setUp(); // TODO: Change the autogenerated stub
        $this->withoutExceptionHandling();
        $this->vehicule = create("App\Vehicule");


    }


    /** @test */
    function  un_utilisateur_non_authentifie_ne_peut_pas_ajouter_une_visite_technique_a_un_vehicule()
    {

        $this->expectException('Illuminate\Auth\AuthenticationException');

        $visitetechnique = make('App\VisiteTechnique',['vehicule_id'=>$this->vehicule->id]);


        $this->post('/visites-techniques', $visitetechnique->toArray());
    }

    /** @test */
    function un_utilisateur_authentifie_peut_ajouter_une_visite_technique_a_un_vehicule()
    {

        $this->signInAPI();

        $this->assertDatabaseMissing("visite_techniques",['vehicule_id'=>$this->vehicule->id]);
        //on a un vehicule
        $visitetechnique = make('App\VisiteTechnique',['vehicule_id'=>$this->vehicule->id]);

        $response = $this->json("post",'/visites-techniques',$visitetechnique->toArray());

         $this->assertDatabaseHas("visite_techniques",['vehicule_id'=>$this->vehicule->id]);
    }
}
