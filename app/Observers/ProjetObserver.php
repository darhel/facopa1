<?php

namespace App\Observers;

use App\Projet;
use App\FluxFinance;
use Carbon\Carbon;

class ProjetObserver
{
    /**
     * Handle the Projet "created" event.
     *
     * @param  \App\Projet  $Projet
     * @return void
     */
    public function created(Projet $Projet)
    {

    }

    /**
     * Handle the Projet "updated" event.
     *
     * @param  \App\Projet  $Projet
     * @return void
     */
    public function updated(Projet $Projet)
    {

    }

    /**
     * Handle the Projet "deleted" event.
     *
     * @param  \App\Projet  $Projet
     * @return void
     */
    public function deleted(Projet $Projet)
    {


    }

    /**
     * Handle the Projet "restored" event.
     *
     * @param  \App\Projet  $Projet
     * @return void
     */
    public function restored(Projet $Projet)
    {
        //
    }

    /**
     * Handle the Projet "force deleted" event.
     *
     * @param  \App\Projet  $Projet
     * @return void
     */
    public function forceDeleted(Projet $Projet)
    {
        //
    }
}
