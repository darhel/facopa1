<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use DB;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dateEncours = Carbon::now();
      

        return [
            "ca"=>0,
            "vpp"=>0,
            "cpp"=>0,
            "vmp"=>0,
            "vehicules_count" => 0,
            "historiques" =>[],
            "clients_count" => 0,
            "locations_count" => 0,
            "locations_en_cours_count" => 0,
            "locations_annulees_count" => 0,
            "users_count" => 0,
            "locations" => [] ,
            "finances" => [],


        ];
    }

}
