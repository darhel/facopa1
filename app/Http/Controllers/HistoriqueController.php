<?php

namespace App\Http\Controllers;

use App\Exports\HistoriqueExport;
use App\Historique;
use App\Http\Controllers\Controller;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Spatie\Activitylog\Models\Activity;

class HistoriqueController extends Controller
{
        /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $per = request()->query("per_page") && is_numeric(request()->query("per_page")) ? request()->query("per_page") : 10 ;
        $q = request()->query('filter') == null ? null : request()->query('filter');
        $utilisateur = request()->query("filter_utilisateur") && is_numeric(request()->query("filter_utilisateur")) ? request()->query("filter_utilisateur") : null  ;
        $d_fin = request()->query('filter_date_fin') == null ? null : request()->query('filter_date_fin');
        $d_dbt = request()->query('filter_date_dbt') == null ? null : request()->query('filter_date_dbt');

        if($utilisateur)
        {
            $utilisateur = User::whereId($utilisateur)->first();
        }

        return    Historique::with(['causer','subject'])
                    ->search($q)
                    ->causedBy($utilisateur)
                    ->periode($d_dbt,$d_fin)
                    ->orderBy('activity_log.created_at','desc')->paginate($per);


    }

    public function export()
    {

        // $q = request()->query('filter') == null ? null : request()->query('filter');
        // $d_fin = request()->query('filter_date_fin') == null ? null : request()->query('filter_date_fin');
        // $d_dbt = request()->query('filter_date_dbt') == null ? null : request()->query('filter_date_dbt');
        // $annee= request()->query('annee') == null ? null : request()->query('annee');
        // $mois = request()->query('mois') == null ? null : request()->query('mois');
        // $vehicule = request()->query('vehicule_id') == null ? null : request()->query('vehicule_id');

        $format = request()->query('format') == null ? null : request()->query('format');
        $q = request()->query('filter') == null ? null : request()->query('filter');
        $d_fin = request()->query('filter_date_fin') == null ? null : request()->query('filter_date_fin');
        $d_dbt = request()->query('filter_date_dbt') == null ? null : request()->query('filter_date_dbt');
        $utilisateur = request()->query('utilisateur') == null ? null : request()->query('utilisateur');
        $filename = "historiques-".Carbon::now()->toDateTimeString();
        if($utilisateur)
        {
            $utilisateur = User::whereId($utilisateur)->first();
        }
        if($format)
        {
           if($format=="excel")
           return (new HistoriqueExport($q,$d_dbt,$d_fin,$utilisateur))->download($filename.'.xlsx');
        }

    }
}
