<?php

namespace App\Http\Controllers;

use DB;
use Log;
use App\User;
use App\Membre;
use App\Projet;
use App\Adresse;
use App\Contact;
use Carbon\Carbon;
use App\Traits\CanUpload;
use Illuminate\Http\Request;
use App\Exports\MembreExport;
use App\Http\Controllers\Controller;
use App\Http\Requests\MembreRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Log as FacadesLog;

class MembreController extends Controller
{

    use CanUpload;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pro = Projet::all()->take(1);
        $per = request()->query("per_page") && is_numeric(request()->query("per_page")) ? request()->query("per_page") : 10 ;
        $q = request()->query('filter') == null ? null : request()->query('filter');
        $p = request()->query('filter_pays') == null ? null : request()->query('filter_pays');
        $pro = request()->query('filter_promotion') == null ? null : request()->query('filter_promotion');
        $secteur = request()->query('filter_secteur') == null ? null : request()->query('filter_secteur');
        return   User::with(['nationalites','competences',"residence","secteur","projets","entreprises"])->search($q)
                        ->promotion($pro)
                        ->secteur($secteur)
                        ->where('type','membre')
                        ->whereHas('nationalites', function($query) use ($p) {
                            if($p!=null)
                            $query->where('nationalite_membre.nationalite_id',$p);
                        })
                        ->orderBy("users.created_at",'desc')
                        ->paginate(8);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getAsParams()
    {
        $q = request()->query('query') == null ? null : request()->query('query');

        return  $q ?  User::select('id','users.nom','users.prenom')->searchAsParameter($q)->where('type','membre')->paginate(8) : null ;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MembreRequest $request)
    {
        Log::info("Store debut");
        Log::info($request->input('step'));

        if($request->input('step')==2 || $request->input('step')=="2")
        {
            Log::info("Store xxxx");

            try
            {
                DB::beginTransaction();
                //Creation de l'adresse
                //Ajout de l'image
                $fileName = null;
                if($request->input('avatar')!="" && $request->input('avatar')!=null)
                {
                    $fileName =  $this->uploadAvatar($request->input('avatar'),$this->getMime($request->input('avatar')));
                }

                $membre = User::create(
                    [
                        'nom' =>$request->input('nom'),
                        'civilite' =>$request->input("civilite"),
                        'statut_marital' =>$request->input("statut_marital"),
                        'prenom' =>$request->input('prenom'),
                        'avatar' =>$fileName ,
                        'date_naissance' =>$request->input('date_naissance'),
                        'promotion' =>$request->input('promotion'),
                        'description' =>$request->input('description'),
                        'secteur_activite_id' =>$request->input('secteur_activite_id'),
                        'formation_id' =>$request->input('formation_id'),
                        'derniere_profession' =>$request->input('derniere_profession'),
                        'activite_actuelle' =>$request->input('activite_actuelle'),
                        'pays_residence_id' =>$request->input('pays_residence_id'),
                        'phone1' =>$request->input('phone1'),
                        'phone2'=>$request->input('phone2'),
                        'siteweb'=>$request->input('siteweb'),
                        'type'=>'membre',
                        'email'=>$request->input('email'),
                        'facebook'=>$request->input('facebook'),
                        'twitter'=>$request->input('twitter'),
                        'linkedin'=>$request->input('linkedin'),
                        'created_by'=>Auth::user()->id,
                        'password'=>Hash::make($request->input('password')),
                    ]
                    );

                //ajout des nationalites
                $membre->nationalites()->attach($request->input('nationalites'));
                $membre->competences()->attach($request->input('competences'));



                Log::info($membre);

                DB::commit();
                return response()->json(['success' => true,'membre'=> $membre->load(['nationalites','competences',"residence","secteur"])],201);

            }catch(\Exception $e)
            {
                DB::rollback();
                Log::info($e->getMessage());
                return response()->json(['success' => false,"message"=>$e->getMessage()],201);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Membre  $Membre
     * @return \Illuminate\Http\Response
     */
    public function show(User $membre)
    {
        return $membre->load(['nationalites','competences',"residence","secteur"]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Membre  $Membre
     * @return \Illuminate\Http\Response
     */
    public function edit(Membre $Membre)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Membre  $Membre
     * @return \Illuminate\Http\Response
     */
    public function update(MembreRequest $request, User $membre)
    {
         Gate::authorize('update', $membre);
        if($request->input('step')==2 || $request->input('step')=="2")
        {


        try
        {
            DB::beginTransaction();

            if($request->input('avatar')!="")
            {
                //on upload le permis
                $fileName =  $this->uploadAvatar($request->input('avatar'),$this->getMime($request->input('avatar')));
                $membre->avatar =$fileName ;
            }
            $membre->nom =$request->input("nom");
            $membre->civilite =$request->input("civilite");
            $membre->statut_marital =$request->input("statut_marital");
            $membre->prenom =$request->input("prenom");

            $membre->date_naissance =$request->input("date_naissance");
            $membre->promotion =$request->input("promotion");
            $membre->description =$request->input("description");
            $membre->secteur_activite_id =$request->input("secteur_activite_id");
            $membre->formation_id =$request->input("formation_id");
            $membre->derniere_profession =$request->input("derniere_profession");
            $membre->activite_actuelle =$request->input("activite_actuelle");
            $membre->pays_residence_id =$request->input("pays_residence_id");
            $membre->phone1 =$request->input("phone1");
            $membre->phone2=$request->input("phone2");
            $membre->siteweb=$request->input("siteweb");
            $membre->email=$request->input("email");
            $membre->facebook=$request->input("facebook");
            $membre->twitter=$request->input("twitter");
            $membre->linkedin=$request->input("linkedin");
            $membre->password=Hash::make($request->input('password'));

            $membre->save();

            $membre->nationalites()->sync($request->input('nationalites'));
            $membre->competences()->sync($request->input('competences'));

            DB::commit();
            return response()->json(['success' => true,'membre'=> $membre->load(['nationalites','competences',"residence","secteur"])],200);

        }catch(\Exception $e)
        {
            DB::rollback();
            return response()->json(['success' => false,"message"=>$e->getMessage()],201);
        }
      }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Membre  $Membre
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $membre)
    {
        Gate::authorize('delete', $membre);

        $membre->delete();
        return response()->json(['success' => true,"membre"=>$membre],200);
    }

    public function getFile($id)
    {
        $Membre = User::whereId($id)->first();
        $path = storage_path("/app/public/documents/identites/".$Membre->piece_identite);


        if (file_exists($path))
        {
            ob_end_clean();
            return response()->download($path);
        }
        else
        {
            //abort(500, 'Something went wrong');
            return back()->with('status', 'La pièce de ce Membre est inexistante')
            ->with('type', 'error');
        }
        //return response()->download($path);
    }
    public function export()
    {

        $q = request()->query('filter') == null ? null : request()->query('filter');
        $format = request()->query('format') == null ? null : request()->query('format');
        $filename = "Membres-".Carbon::now()->toDateTimeString();
        if($format)
        {
           if($format=="excel")
           return (new MembreExport($q))->download($filename.'.xlsx');
           if($format=="pdf")
           return Excel::download(new MembreExport($q),$filename.'.pdf');
        }

    }
}
