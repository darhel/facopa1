<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\Traits\CanTrackHistory;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Spatie\Activitylog\Contracts\Activity;

class MembreLoginController extends Controller
{

    use AuthenticatesUsers;
    protected $redirectTo = RouteServiceProvider::HOME;

   public function login(Request $request){

    $credentials = $request->only('email', 'password');
    if (Auth::guard('admins')->attempt(['email'=>$request->email,'password'=>$request->password])) {
        return redirect()->intended('/');
    }
   }
}
