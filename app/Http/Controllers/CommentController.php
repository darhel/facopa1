<?php

namespace App\Http\Controllers;

use App\Comment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\CommentRequest;
use Illuminate\Support\Facades\Gate;


class CommentController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CommentRequest $request)
    {
        try
        {

            DB::beginTransaction();

            $comment = Comment::create([
                'projet_id' => $request->projet_id,
                'content' => $request->content,
                'membre_id' => Auth::user()->id,
            ]);
            DB::commit();
            return response()->json(['success' => true,'comment'=> $comment->load(['user'])],200);
        }
        catch(\Exception $e)
        {
            DB::rollback();
            return ['status'=>false,'message'=>$e->getMessage()];
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //on supprime
        $comment = Comment::findOrFail($id);
        Gate::authorize('delete',$comment );
            $comment->delete();
        return response()->json(['message' => 'Commentaire supprimé avec succès'],200);

    }
}
