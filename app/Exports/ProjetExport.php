<?php

namespace App\Exports;

use App\Projet;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithTitle;

class ProjetExport implements WithTitle, FromQuery,WithMapping,WithHeadings,WithColumnFormatting,ShouldAutoSize
{

    use Exportable; 
    /**
    * @return \Illuminate\Support\Collection
    */
    public function query()
    {
        ob_end_clean(); // this
        ob_start(); // and this
        return projet::query();
    }

    public function title(): string
    {
        return 'projets';
    }
 
    //use Exportable; 

    public function map($projet): array
    {
        return [
            $projet->date_facturation,
            $projet->description,
            $projet->montant,
            $projet->vehicule->libelle,
            $projet->type_projet->libelle,


        ];
    }

    public function headings(): array
    {
        return [
            "Date de facturation ",
            "Description",
            'Montant',
            'Voiture Concerné',
            'Type de dépense'
        ];
    }

    public function columnFormats(): array
    {
        return [

        ];
    }
}
