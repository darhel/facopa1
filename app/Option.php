<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Spatie\Activitylog\Traits\LogsActivity;

class Option extends Model
{
    use LogsActivity;

    protected static $logAttributes = ["type_option_id","libelle"];
    protected static $logName = 'option';
    protected static $logOnlyDirty = true;   protected static $submitEmptyLogs = false;



 



    public function scopeSearch($query, $q)
    {
        if ($q == null) return $query;
        return $query
                ->orWhere('options.libelle', 'LIKE', "%{$q}%")
                ->orWhere('options.created_at', 'LIKE', "%{$q}%")
                ->orWhere('type_options.libelle', 'LIKE', "%{$q}%")
                ->join('type_options', 'type_options.id', '=', 'options.type_option_id');;
    }
}
