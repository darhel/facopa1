<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Spatie\Activitylog\Contracts\Activity;
use Spatie\Activitylog\Traits\LogsActivity;

class Role extends Model
{
    use LogsActivity;

    protected $guarded=[];
    protected static $logAttributes = ["nom","libelle"];
    protected static $logName = 'role';
    protected static $logOnlyDirty = true;   protected static $submitEmptyLogs = false;


    public function tapActivity(Activity $activity, string $eventName)
    {
        $activity->description = "{$eventName}";
        if($eventName=="deleted")
        {
            $activity->as_yourself = "Vous avez supprimé le rôle <strong>{$this->nom}</strong>";
            $activity->as_someone_else = ucFirst(Auth::check() ? Auth::user()->name : "Le système")." a supprimé le rôle <strong>{$this->nom}</strong>";
        }
        elseif($eventName=="updated")
        {
            $activity->as_yourself = "Vous avez modifié  le rôle <strong>{$this->nom}</strong>";
            $activity->as_someone_else = ucFirst(Auth::check() ? Auth::user()->name : "Le système")." a modifié le rôle <strong>{$this->nom}</strong>";
        }
        else
        {
            $activity->as_yourself = "Vous avez ajouté  le rôle <strong>{$this->nom}</strong>";
            $activity->as_someone_else = ucFirst(Auth::check() ? Auth::user()->name : "Le système")." a ajouté  le rôle <strong>{$this->nom}</strong>";
        }
        
    }
    public function permissions()
    {
        return $this->belongsToMany("App\Permission",'permission_role');
    }

    public function hasPermissions()
    {
      return $this->permissions->count() > 0 ? true : false ;
    }


    
    public function scopeSearch($query, $q)
    {
        if ($q == null) return $query;
        return $query
                ->orWhere('libelle', 'LIKE', "%{$q}%")
                ->orWhere('created_at', 'LIKE', "%{$q}%");
    }
}
